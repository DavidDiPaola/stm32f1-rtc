# rtc
stm32f1 real time clock library

[GitLab](https://gitlab.com/DavidDiPaola/stm32f1-rtc)
[GitHub](https://github.com/DavidDiPaola/stm32f1-rtc)

to be used with [stm32f1-common](https://gitlab.com/DavidDiPaola/stm32f1-common)

